"""
@author: JD+AV

Modifications by EL
"""

#from __future__ import unicode_literals

import sys #, scipy.io
import os, time, h5py, json
import hdf5plugin
import numpy as np
#from  silx.opencl.backprojection import Backprojection as fbp
from skimage.transform import resize
from skimage.transform import rotate
from skimage.transform import radon, iradon

import scipy.optimize as sciopt
import pickle

from  silx.opencl.backprojection import Backprojection as fbp

from tqdm import tqdm

import pdb

class Slice:
    def __init__(self,filename, transmission=[]):
        self.filename = filename
        self.transmission = transmission
        if isinstance(self.filename, dict):
            self._file_merge()
        else: 
            self._file_open()
        self.int = {}
        self.polyorder = 1
        self.calcbackground = False
        self.samplesize = 40

    def _iterate_dict(self, var):
        for index in range(Dict[0].var.shape[0]):
            for dict_numb in range(len(Dict)):
                self.var[int(index*len(Dict)+dict_numb)] = Dict[dict_numb][index][:]
        
    def _file_open(self):
        print("Loading Slice normally")
        hdf_fileName = self.filename
        self.sinos = np.zeros(())
        selfbp = np.zeros(())
        if len(hdf_fileName)>0:
            if (hdf_fileName[-2:] == 'h5'): 
                with h5py.File(hdf_fileName,'r') as f:
                    try:
                        self.q = f['/q'][:]
                        self.th = f['/th'][:]
                        self.y = f['/y'][:]
                        self.data = f['/XRD'][:]
                    except:
                        print("Something didn't load")
                        pass
                    
                    self.tth = self.q
                
                self.na = len(set(self.th))
                self.nt = len(self.data)/self.na
                
                #self.na = 1
                if self.transmission != []:
                    for n in range(self.data.shape[0]):
                        self.data[n] = self.data[n] * self.transmission[n]
                        
                self.data = np.reshape(self.data,(int(self.na),int(self.nt),self.data.shape[1]))
                self.data = np.transpose(self.data,(1,0,2))
                self.data[:,0::2,:] = self.data[::-1,0::2,:]
                self.sinos = self.data
                self.sinos_orig = np.copy(self.sinos)
                self.scantype = 'zigzag'
                self.maxTh = max(self.th)-min(self.th)
    
    def _file_merge(self):
        print("Merging Slices")
        
        Dict = self.filename
        self.sinos = np.zeros(())
        selfbp = np.zeros(())
        
        self.q = Dict[0].q
        
        self.th = np.zeros(Dict[0].th.shape[0]*len(Dict))
        for index in range(Dict[0].th.shape[0]):
                for dict_numb in range(len(Dict)):
                    self.th[int(index*len(Dict)+dict_numb)] = Dict[dict_numb].th[index]
        
        self.y = np.zeros(Dict[0].y.shape[0]*len(Dict))
        for index in range(Dict[0].y.shape[0]):
                for dict_numb in range(len(Dict)):
                    self.y[int(index*len(Dict)+dict_numb)] = Dict[dict_numb].y[index]
        
        self.data = np.zeros((Dict[0].data.shape[0], Dict[0].data.shape[1]*len(Dict), Dict[0].data.shape[2]))
        for index in range(Dict[0].data.shape[1]):
                for dict_numb in range(len(Dict)):
                    self.data[:,int(index*len(Dict)+dict_numb)] = Dict[dict_numb].data[:,index]
        
        self.tth = self.q
                    
        self.na = len(set(self.th))
        self.nt = len(self.data)/self.na
        self.sinos = self.data
        self.sinos_orig = np.copy(self.sinos)
        self.scantype = 'zigzag'
    
    def _file_open_Raph(self):
        hdf_fileName = self.filename
        self.sinos = np.zeros(())
        selfbp = np.zeros(())
        if len(hdf_fileName)>0:
            if (hdf_fileName[-2:] == 'h5'): 
                with h5py.File(hdf_fileName,'r') as f:
                    try:
                        self.q = f['/q'][:]
                        self.th = f['/th'][:]
                        self.y = f['/y'][:]
                        self.data = f['/XRD'][:]
                    except:
                        pass
                    
                    self.tth = self.q
                self.na = 160
                self.nt = 161                
                #self.na = len(set(self.th))
                #self.nt = len(self.data)/self.na
                #self.na = 1          
                self.data = np.reshape(self.data,(int(self.nt),int(self.na),self.data.shape[1]))
               
                self.data = self.data[::,4:152,:]  #translation, rotation, q
                print(self.data.shape)
                
                self.sinos = self.data
                self.sinos_orig = np.copy(self.sinos)
                self.scantype = 'forward'
                   
                   
    def sinoproc(self): #(self,s):    

        self.sinos = np.copy(self.sinos_orig)
        self.ProcSinos = SinoProcessing(self.sinos,self.sinonorm,self.ofs,self.crsr,self.scantype, self.wiggle)
        

    def fbprec_vol(self): #(self,s):    

        try:
            self.sinos = self.ProcSinos.sinos_proc
        except:
            self.sinos = self.data

        self.Rec = ReconstructData(self.sinos, self.maxTh, self.q, self.calcbackground, self.polyorder, self.samplesize) # or self.data
        
    def integrate(self,name,qrange):
        
        if self.Rec:
            try:
                self.int[name] = IntegrateData(self.q, self.Rec.bp, qrange, self.Rec.backfits, self.polyorder, name)
            except:
                self.int[name] = IntegrateData(self.q, self.Rec.bp, qrange, 0, self.polyorder, name)
            
            
    def save_XRDCT(self,folder,filename):
        
        fn = os.path.splitext(filename)[0]+'_xrdct.h5'
        self.fn_XRDCT = os.path.join(folder,fn)
        
        h5f = h5py.File(self.fn_XRDCT, "w")
        h5f.create_dataset('Sinograms', data=self.sinos)
        h5f.create_dataset('Reconstructed', data=self.Rec.bp)
        h5f.create_dataset('twotheta', data=self.tth)
        h5f.create_dataset('q', data=self.q)
        h5f.close()

        perm = 'chmod 777 %s' %self.fn_XRDCT
        os.system(perm)
        
    def save_instance(self,folder,filename):  # saves pickle
        
        self.fn_pickle = os.path.join(folder,filename)
        
        #check if the filename is gz
                    
        from compress_pickle import dump
        
        dump(self,self.fn_pickle)   
        
    def save_all_int_h5(self,folder,filename):
        
        dataset = {}
        fn = os.path.splitext(filename)[0]+'_integrations.h5'
        self.fn_intall = os.path.join(folder,fn)
        h5 = h5py.File(self.fn_intall,'w')
        
        for name in self.int.keys():
              dataset[name] = h5.create_dataset(name,
                                               np.shape(self.int[name].integrated),
                                               data = self.int[name].integrated)
        h5.close()
        
        perm = 'chmod 777 %s' %self.fn_intall
        os.system(perm)
        
        
class SinoProcessing():
    #snprocdone = QtCore.pyqtSignal()
    #progress_sino = QtCore.pyqtSignal(int)
    
    def __init__(self,sinos,sinonorm,ofs,crsr,scantype, wiggle): #sinonorm - normlaize?, ofs - offset?, ...
        #QtCore.QThread.__init__(self)
        self.sinos_proc = sinos
        self.sinonorm = sinonorm
        self.ofs = ofs
        self.crsr = crsr
        self.scantype = scantype
        self.wiggle = wiggle
        
        
    def run(self):
        
        if self.sinonorm == 1:
            self.scalesinos()   
        
        if self.ofs>0:
            self.airrem()

        if self.crsr>0:       
            self.sinocentering()
        
        if self.wiggle == 1:
            self.wiggle_correction()
        
        print(type(self.sinos_proc[0,0,0]))
        
    def airrem(self):
        
        print("Running airrem")
        for ii in tqdm(range(0,self.sinos_proc.shape[1])):
            dpair = (np.mean(self.sinos_proc[0:self.ofs,ii,:],axis = 0) + np.mean(self.sinos_proc[self.sinos_proc.shape[0]-self.ofs:self.sinos_proc.shape[0],ii,:],axis = 0))/2
            self.sinos_proc[:,ii,:] = self.sinos_proc[:,ii,:] - dpair    
        print("airrem done")
        
    def scalesinos(self):
        """
        From nDTomo Documentation (Which seems to be the source of the Code):
        
        Method for normalising a sinogram volume (translations x projections x nz/spectral).
        It assumes that the total intensity per projection is constant.
        Dimensions: translation steps (detector elements), projections, z (spectral)
        """
        print("Running scalesinos")
        ss = np.sum(self.sinos_proc,axis = 2)
        scint = np.zeros((self.sinos_proc.shape[1]))
        # Summed scattering intensity per linescan
        for ii in range(0,self.sinos_proc.shape[1]):
            scint[ii] = np.sum(ss[:,ii])
        # Scale factors
        sf = scint/np.max(scint)
        
        # Normalise the sinogram data    
        for jj in tqdm(range(0,self.sinos_proc.shape[1])):
            self.sinos_proc[:,jj,:] = self.sinos_proc[:,jj,:]/sf[jj] 
        
        print('scalesinos done')
        
    def wiggle_correction(self):
        """
        Wiggle correction algorithm based on center of mass to correct wiggle observed (and center sinogram)
        """
        
        def Mass_Centering(Sinostripe):
            """
            Takes Stripe of Sinogram, finds Center of Mass by sum(value*coordinate)/sum(values) and shifts the stripe
            to match the actual center (len/2).
            Returns shifted stripe
            """
            Center = 0
            Sum = 0
            
            if len(Sinostripe.shape) > 1:
                Stripe = np.sum(Sinostripe, 1)
            else:
                Stripe = Sinostripe
            
            Stdv = np.std(Stripe)
            Mean = np.mean(Stripe)
            
            for i in range(len(Stripe)):
                numb = Stripe[i]
                
                if abs(numb-Mean) > 2*Stdv:
                    try: 
                        numb = (Stripe[i-1] - Stripe[i+1]) / 2
                    except:
                        numb = Mean
                        
                Center += (numb*i)
                Sum += numb
            Center = Center / Sum
            diff = (len(Stripe)/2) - Center
            diff = int(np.rint(diff))
    
            Copy = np.zeros_like(Sinostripe)
            
            for layer in range(Sinostripe.shape[1]):
                for i in range(Sinostripe.shape[0]-abs(diff)):
                    Copy[i+diff, layer] = Sinostripe[i, layer]
                
            return Copy
        
        Sino_2d = np.sum(self.sinos_proc,axis = 2)
        corrected = np.zeros_like(self.sinos_proc)
        
        count = 0
        for stripe in range(Sino_2d.shape[1]):
            self.sinos_proc[:,stripe] = Mass_Centering(self.sinos_proc[:,stripe])
            count += 1
            if count == 25:
                count = 0
                print("At stripe "+str(stripe)+" of "+str(Sino_2d.shape[1]))

        
    def sinocentering(self):
        """
        Added comments to understand what this is trying to do
        -Erik
        
        After going through it:
        First step is to try shifts of the sinogram up/down in the range given by the passed number
        --> Calculates the Stdv between first and inverted last Sinostripe and takes the shift with the smallest
        
        Then interpolates the sino Volume with the determined shift
        
        But it looks like it never actually inverts the zigzag scans??
        """
        print("Running Sinocentering")
        #Checks for Dimensions of Data, if Volume --> summed to "classic" Sinogram       
        di = self.sinos_proc.shape
        if len(di)>2:
            s = np.sum(self.sinos_proc, axis = 2)
        
        #Makes array of around middle in range of input number with 0.1 Stepsize
        cr =  np.arange(s.shape[0]/2 - self.crsr, s.shape[0]/2 + self.crsr, 0.1)
    #    nomega = s.shape[1] - 1
        
        #Array of "old" coordinate list
        xold = np.arange(0,s.shape[0])
        
        #Creates empty lists for standard deviations and corresponding indices
        st = []; ind = [];
        
        for kk in range(0,len(cr)):
            
            #Makes coordinate system shifted by input number points? --> Generates zero array with same size as sino
            xnew = cr[kk] + np.arange(-np.ceil(s.shape[0]/2),np.ceil(s.shape[0]/2)-1)
            sn = np.zeros((len(xnew),s.shape[1]))
            
            #Fills Zero Array colum wise with interpolated values to match the 0.1 stepsize
            for ii in range(0,s.shape[1]):
                
                sn[:,ii] = np.interp(xnew, xold, s[:,ii], left=0 , right=0)
    

            #Reverses Last Stripe (Why calculate all of them then?), Notes the standard deviation between first and last slice and index. It uses first and last because they should be the same (0 and 180 °) but inverted --> See height offset
            re = sn[::-1,-1]
    #        re = sn[:,:1]
            st.append((np.std((sn[:,0]-re)))); ind.append(kk)
    #        plt.figure(1);plt.clf();plt.plot(sn[:,0]);plt.plot(re);plt.pause(0.001);
    
        #Extracts min of standard devations
        m = np.argmin(st)
        
        #Generates array with size of original data, with the shift it determined
        mm = 0
        xnew = cr[m] + np.arange(-np.ceil(s.shape[0]/2),np.ceil(s.shape[0]/2)-1)
        if len(di)>2:
            sn = np.zeros((len(xnew),self.sinos_proc.shape[1],self.sinos_proc.shape[2]))  
            
            for ll in tqdm(range(0,self.sinos_proc.shape[2])):
                for ii in range(0,self.sinos_proc.shape[1]):
                    sn[:,ii,ll] = np.interp(xnew, xold, self.sinos_proc[:,ii,ll], left=0 , right=0)    
                    
                v = (100.*(mm+1))/self.sinos_proc.shape[2]
                mm = mm + 1
#                self.progress_sino.emit(v)
            
            #Looks like this removes the last entry for every scan if it was zigzag
            if self.scantype == 'zigzag':
                sn = sn[:,0:sn.shape[1]-1,:]
                
#                print ll
                
        elif len(di)==2:
            
            sn = np.zeros((len(xnew),s.shape[1]))    
            for ii in range(0,s.shape[1]):
                sn[:,ii] = np.interp(xnew, xold, s[:,ii], left=0 , right=0)
                
            if self.scantype == 'zigzag':
                sn = sn[:,0:sn.shape[1]-1]
            
        self.sinos_proc = sn
        print("Sincocentering done")
        
    def rot_center(thetasum):

        '''
        Code taken from: https://github.com/everettvacek/PhaseSymmetry
        If used, please cite: J. Synchrotron Rad. (2022). 29, https://doi.org/10.1107/S160057752101277
    
        The method requires that the object is within the field of view at all projection angles so that the reflection pair arcsine distribution is not obscured, 
        and it also assumes that the rotation axis is aligned to the axis of the 2D detector used to acquire projection images
    
        Calculates the center of rotation of a sinogram.
        Parameters
        ----------
        thetasum : array like
            The 2-D thetasum array (z,theta).
        Returns
        -------
        COR : float
            The center of rotation.    
        '''
    
        T = rfft(thetasum.ravel())
        # Get components of the AC spatial frequency for axis perpendicular to rotation axis.
        imag = T[thetasum.shape[0]].imag
        real = T[thetasum.shape[0]].real
        # Get phase of thetasum and return center of rotation.
        phase = np.arctan2(imag*np.sign(real), real*np.sign(real)) 
        COR = thetasum.shape[-1]/2-phase*thetasum.shape[-1]/(2*np.pi)
    
        return COR    

    def zigzag_flip(im):
    
        im = im[:,0:im.shape[1]-1]
    
        return(im)


        
        
        
class ReconstructData():
    #recdone = QtCore.pyqtSignal()
    #progress = QtCore.pyqtSignal(int)
    
    def __init__(self,sinos, maxTh, q, calcbackground, polyorder, samplesize):
        #QtCore.QThread.__init__(self)
        self.sinos = sinos
        self.maxTh = maxTh
        self.q = q
        self.calcbackground = calcbackground
        self.polyorder = polyorder
        self.samplesize = samplesize
        
    def run(self):
        """
        Added notes again
        -Erik
        """
        #Counts number of rotations
        npr = self.sinos.shape[1]
        #Calculates Theta Values. But why not use the ones already there? (slice.th)
        theta = np.arange(0,self.maxTh-self.maxTh/(npr),self.maxTh/(npr-1))  
        #Cuts to length of theta, not sure if necessary and converts to float32
        self.sinos = self.sinos[0:self.sinos.shape[0],0:len(theta),:]
        self.sinos = np.float32(self.sinos)
        #Generates backproject array and runs backprojection
        self.bp = np.zeros((self.sinos.shape[0],self.sinos.shape[0],self.sinos.shape[2]))
        t = fbp(sino_shape=(self.sinos.shape[1],self.sinos.shape[0]),devicetype='all')
#            print self.bp.shape
#            start=time.time()
        ist = 0
        ifi = self.sinos.shape[2]
        #Writes bp volume from returned silx object
        print("Writing Volume")
        for ii in tqdm(range(ist,ifi)):
            self.bp[:,:,ii] = t.filtered_backprojection(\
                sino=np.ascontiguousarray(np.transpose(self.sinos[:,:,ii],(1,0))))
            v = (100.*(ii-ist+1))/(ifi-ist)
#            self.progress.emit(v)

        #Probably runs script to remove ring artifacts
        self.remring()
        
        #Calculate Global Background for each XRD
        if self.calcbackground == True:
            self.background(self.samplesize, self.polyorder)
        
        #self.recdone.emit()

    def remring(self):
        print("Starting remring")
        sz = np.floor(self.bp.shape[0])
        x = np.arange(0,sz)
        x = np.tile(x,(int(sz),1))
        y = np.swapaxes(x,0,1)
        
        xc = np.round(sz/2)
        yc = np.round(sz/2)
        
        r = np.sqrt(((x-xc)**2 + (y-yc)**2));
        
        dim =  self.bp.shape
        if len(dim)==2:
            self.bp = np.where(r>0.98*sz/2,0,self.bp)
        elif len(dim)==3:
            for ii in tqdm(range(0,dim[2])):
                self.bp[:,:,ii] = np.where(r>0.98*sz/2,0,self.bp[:,:,ii]) 
    
    def background(self, samplesize, polyorder):
        
        def test_points(Points):
            for i in range(1, len(Points)-1):
                test = (Points[i-1]+Points[i+1])/2
                if test < Points[i]:
                    Points[i] = test
                else:
                    continue
            return Points
        
        print("Starting Background")
        
        self.backfits = {}
        
        for row in tqdm(range(self.bp.shape[0])):
            for col in range(self.bp.shape[1]):
                XRD = self.bp[row, col]
                Points = np.copy(XRD[45::samplesize])
                Positions = self.q[45::samplesize]
                
                for iteration in range(100):
                    Points_new = test_points(Points)
                    if np.array_equiv(Points, Points_new):
                        break
                    Points = Points_new
                
                self.backfits[row,col] = np.polyfit(Positions, Points, polyorder)

                
class IntegrateData():
    
    def __init__(self, q, bp, qrange, backfits, polyorder, name, remove_negative = True):
        self.q = q
        self.bp = bp
        self.qrange = qrange
        self.polyorder = polyorder
        self.backfits = backfits
        self.name = name
        
    
    def _subBckg(self,xrd):  #subtracts polynomial background. Original Version in tomo_lib        
        idx = self._getIDX(xrd)
        fit = self._fitPoly(idx,xrd)
        fitcurve = np.polyval(fit,self.q[idx[0]:idx[-1]])
        return xrd[idx[0]:idx[-1]]-fitcurve
    
    def _subBckg_globalfit(self,xrd, backfit):  #subtracts global background instead      
        idx = self._getIDX(xrd)
        fitcurve = np.polyval(backfit,self.q[idx[0]:idx[-1]])
        return xrd[idx[0]:idx[-1]]-fitcurve
    
    def _fitPoly(self,idx,xrd):
        return np.polyfit(self.q[idx],xrd[idx],self.polyorder) 
    
    def _getIDX(self,xrd):
        idx =np.asarray([])
        for i, id in enumerate(self.qrange):
            #print(id)
            #print(databcg)
            idxn = np.where(np.logical_and(self.q > id[0], self.q < id[1]))[0]
            idx = np.append(idx,idxn)
        return idx.astype(int)
    
    def run(self):
        bp_shape = np.shape(self.bp)
        self.integrated = np.empty((bp_shape[0],bp_shape[1]))
        for i in tqdm(range(bp_shape[0])):
            for j in range(bp_shape[1]):
                xrd = self.bp[i,j,:]
                if isinstance(self.backfits, dict):
                    backfit = self.backfits[i,j]
                    subtracted = self._subBckg_globalfit(xrd, backfit)
                else:
                    subtracted = self._subBckg(xrd)
                
                self.integrated[i,j] = np.sum(subtracted)
                
                
    def save_ASCII(self,folder,filename):
        
        fn = os.path.splitext(filename)[0]+'_'+self.name+'.dat'
        self.fn_ASCII = os.path.join(folder,fn)
        header = "x y "+"name"
        
        with open(self.fn_ASCII,'w') as f:
            f.write(header)
            f.write('\n')
            
            int_shape = np.shape(self.integrated)
            for i in range(int_shape[0]):
                for j in range(int_shape[1]):
                    f.write('\t'.join([str(i),str(j),str(self.integrated[i,j])]))
                    f.write('\n')
        
    def save_h5(self,folder,filename):
        
        fn = os.path.splitext(filename)[0]+'_'+self.name+'.h5'
        self.fn_h5 = os.path.join(folder,fn)
        h5 = h5py.File(self.fn_h5,'w')
        
        dataset = h5.create_dataset(self.name,np.shape(self.integrated),
                                    data = self.integrated)
        h5.close()
        
        perm = 'chmod 777 %s' %self.fn_h5
        os.system(perm)
            
def save_tomoviz(slices,int_name,folder,filename):
    slice_shape = np.shape(slices[list(slices.keys())[0]].int[int_name].integrated)
    print(slice_shape)
    data_array =  np.empty((slice_shape[0],slice_shape[1],len(slices)))
    
    fn = os.path.splitext(filename)[0]+'_'+int_name+'.h5'
    fn = os.path.join(folder,fn)
    h5 = h5py.File(fn,'w')
    
    for  i,key in enumerate(slices.keys()):
        data_array[:,:,i] = slices[key].int[int_name].integrated
    
    dataset_slices = h5.create_dataset(int_name,data_array.shape,data = data_array)
    h5.close()
    
    perm = 'chmod 777 %s' %h5
    os.system(perm)
    
def load_instance(filename):  # load pickle
        
        from compress_pickle import load
        
        return load(filename)   
    
 
        
        
        
        