import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import h5py
from tqdm import tqdm
from scipy.optimize import curve_fit
from scipy.optimize import fsolve

import tomo_lib_modified as tomo


def get_transmission(h5path, key):
    source = h5py.File(h5path, 'r')
    saxsdiode = source[key+'/saxsdio']
    mondiode = source[key+'/mondio']
    
    saxsnorm = normalize(saxsdiode)
    monnorm = normalize(mondiode)
    
    transmission = saxsnorm/monnorm
    return transmission

def normalize(array):
    norm = np.zeros_like(array)
    maxval = max(array)
    for i in range(norm.shape[0]):
        norm[i] = array[i]/maxval
    return norm

def find_nearest_index(array, value):
    #array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def NMC_max(q, xrd, integrated_point, start, end):
    """Does not work correctly at the moment, probably needs peak fitting or similar approach"""
    #NMC_start = 1.28
    #NMC_end = 1.38
    q_start = find_nearest_index(q, start)
    q_end = find_nearest_index(q, end)
    if integrated_point < 0.9e-9:
        return 0
    else:
        Maxima = np.argmax(xrd[q_start:q_end])
        return q[q_start + Maxima]
    
    
def SAXS_Cut(Saxs_line):
    Longest = [0, 0]
    Current = [0, 0]
    for i, Int in enumerate(Saxs_line[:500]):
        if Int < 1e-9 or i == 499:
            Current[1] = i
            if Current[1]-Current[0] > Longest[1]-Longest[0]:
                Longest = Current
            Current = [i+1, 0]
        else:
            continue
    Longest[0] = Longest[0] + 2
    Longest[1] = Longest[1] - 2
    return Longest

#q over x function to be optimized
def qx(x, alpha, d):
    return (x**(-alpha)/d)

def qx_fit(Cut_q, Cut_I):
    popt, pcov = curve_fit(qx, Cut_q, Cut_I)
    return popt, pcov

def q4(x, d):
    return (x**(-4)/d)

def q4_fit(Cut_q, Cut_I):
    popt, pcov = curve_fit(q4, Cut_q, Cut_I)
    return popt, pcov

def get_intercept(Fit1,Fit2, x0=0.04):
    Res= fsolve(lambda x : qx(x, *Fit1) - qx(x, *Fit2),x0)
    return Res

